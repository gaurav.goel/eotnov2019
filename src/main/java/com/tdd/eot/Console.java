package com.tdd.eot;

import java.util.Scanner;

public class Console {
    public String readLine() {
        return new Scanner(System.in).nextLine();
    }
}
