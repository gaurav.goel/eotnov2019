package com.tdd.eot;

import java.util.Objects;

public class Score {
    private final int player1Score;
    private final int player2Score;

    public Score(int player1Score, int player2Score) {
        this.player1Score = player1Score;
        this.player2Score = player2Score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Score score = (Score) o;
        return player1Score == score.player1Score &&
                player2Score == score.player2Score;
    }

    @Override
    public int hashCode() {
        return Objects.hash(player1Score, player2Score);
    }

    @Override
    public String toString() {
        return "Score{" +
                "player1Score=" + player1Score +
                ", player2Score=" + player2Score +
                '}';
    }

    public Score add(Score other) {
        return new Score(this.player1Score + other.player1Score,
                                this.player2Score + other.player2Score);
    }
}
