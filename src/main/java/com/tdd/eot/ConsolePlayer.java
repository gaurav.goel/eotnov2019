package com.tdd.eot;

import java.util.Scanner;

public class ConsolePlayer extends Player {
    private final Console console;

    public ConsolePlayer(Console console) {
        this.console = console;
    }

    @Override
    public Move makeMove() {
        return console.readLine().equals("CH")?Move.CHEAT:Move.COOPERATE;
    }
}
