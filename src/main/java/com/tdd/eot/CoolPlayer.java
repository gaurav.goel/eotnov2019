package com.tdd.eot;

public class CoolPlayer extends Player {
    @Override
    public Move makeMove() {
        return Move.COOPERATE;
    }
}
