package com.tdd.eot;

public class CheatingPlayer extends Player {
    @Override
    public Move makeMove() {
        return Move.CHEAT;
    }
}
