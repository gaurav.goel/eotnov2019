package com.tdd.eot;

import static com.tdd.eot.Move.CHEAT;
import static com.tdd.eot.Move.COOPERATE;

public class RuleProcessor {
    public Score process(Move player1Move, Move player2Move) {
        if(player1Move==CHEAT && player2Move==CHEAT) return new Score(0,0);
        if(player1Move==COOPERATE && player2Move==CHEAT) return new Score(-1,3);
        if(player1Move==COOPERATE && player2Move==COOPERATE) return new Score(2,2);
        if(player1Move==CHEAT && player2Move==COOPERATE) return new Score(3,-1);

        throw new RuntimeException("Should not reach here");
    }

    private int poolContribution(Move move) {
        return (move==COOPERATE)?1:0;
    }
}
