package com.tdd.eot;

import static com.tdd.eot.Move.COOPERATE;

public class CopycatPlayer extends Player implements MoveListener{
    private Move lastMove = COOPERATE;

    @Override
    public Move makeMove() {
        return lastMove;
    }

    @Override
    public void update(Move player1Move, Move player2Move) {
        lastMove = player1Move==lastMove?player2Move:player1Move;
    }
}
