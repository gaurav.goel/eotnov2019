package com.tdd.eot;

import org.junit.Test;

import static com.tdd.eot.Move.CHEAT;
import static com.tdd.eot.Move.COOPERATE;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class GameTest {

    @Test
    public void shouldPlayTwoRoundsWithCheatingAndCoolPlayer(){
        Player player1 = mock(Player.class);
        Player player2 = mock(Player.class);
        RuleProcessor ruleProcessor = mock(RuleProcessor.class);

        when(player1.makeMove()).thenReturn(COOPERATE);
        when(player2.makeMove()).thenReturn(CHEAT);
        when(ruleProcessor.process(COOPERATE, CHEAT)).thenReturn(new Score(-1,3));

        Game game = new Game(player1, player2, ruleProcessor);
        Score score = game.play(2);

        assertEquals(new Score(-2, 6), score);
        verify(player1, times(2)).makeMove();
        verify(player2, times(2)).makeMove();
    }

    @Test
    public void shouldInformMoveListenerAboutPreviousRoundMove(){
        Player player1 = mock(Player.class);
        Player player2 = mock(Player.class);
        MoveListener moveListener = mock(MoveListener.class);
        RuleProcessor ruleProcessor = mock(RuleProcessor.class);

        when(player1.makeMove()).thenReturn(COOPERATE);
        when(player2.makeMove()).thenReturn(CHEAT);
        when(ruleProcessor.process(any(), any())).thenReturn(new Score(5,5));

        Game game = new Game(player1, player2, ruleProcessor);
        game.addMoveListener(moveListener);
        game.play(1);
        verify(moveListener).update(COOPERATE, CHEAT);

    }

}
