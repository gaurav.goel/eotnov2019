package com.tdd.eot;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ConsolePlayerTest {

    @Test
    public void shouldReadCheatInputFromConsole(){
        Console console = mock(Console.class);
        when(console.readLine()).thenReturn("CH");
        ConsolePlayer consolePlayer = new ConsolePlayer(console);
        assertEquals(Move.CHEAT, consolePlayer.makeMove());

    }

    @Test
    public void shouldReadCooperateInputFromConsole(){
        Console console = mock(Console.class);
        when(console.readLine()).thenReturn("CO");
        ConsolePlayer consolePlayer = new ConsolePlayer(console);
        assertEquals(Move.COOPERATE, consolePlayer.makeMove());

    }
}
