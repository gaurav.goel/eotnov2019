package com.tdd.eot;

import org.junit.Test;

import static com.tdd.eot.Move.CHEAT;
import static com.tdd.eot.Move.COOPERATE;
import static org.junit.Assert.assertEquals;

public class GameIntegrationTest {
    @Test
    public void shouldCalculateScoreForThreeRounds(){
        Player player1 = new Player(CHEAT, COOPERATE, CHEAT);
        Player player2 = new Player(COOPERATE, COOPERATE, CHEAT);
        Game game = new Game(player1, player2, new RuleProcessor());
        Score score = game.play(3);
        assertEquals(new Score(5, 1), score);
    }

    @Test
    public void shouldCalculateScoreForOneCopycatAndOneAlwaysCheat(){
        Player player1 = new CheatingPlayer();
        CopycatPlayer player2 = new CopycatPlayer();
        Game game = new Game(player1, player2, new RuleProcessor());
        game.addMoveListener(player2);
        Score score = game.play(3);
        assertEquals(new Score(3, -1), score);

    }
}
