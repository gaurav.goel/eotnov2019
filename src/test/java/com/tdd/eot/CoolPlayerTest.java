package com.tdd.eot;

import org.junit.Test;

import static com.tdd.eot.Move.COOPERATE;
import static org.junit.Assert.assertEquals;

public class CoolPlayerTest {
    @Test
    public void shouldAlwaysCooperate() {
        CoolPlayer coolPlayer = new CoolPlayer();
        assertEquals(COOPERATE, coolPlayer.makeMove());
    }
}
